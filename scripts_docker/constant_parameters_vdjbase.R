#!/usr/bin/env Rscript
############################################# Constant parameters ###############################################

### Realign using personal reference and IgBlast
# IgBlast directory
igblast_dir <-  '/usr/local/bin/'  ## change to your igblast path
IGDATA <- '/usr/local/share/igblast' ## change to your igblast directory

# collapse seq script path 
newcollapse_script_path <- "/usr/local/bin/collapse_seq.py" ## change to script location.

# customize tigger novel allele function
source("/usr/local/bin/functions_tigger.R") ## change to script location.


GERM_PATH <-
  paste0("/usr/local/bin/igblast_reference/human", chain) ### chage to your germline folder path

short_ref <- ifelse(short, "_bp", "_gap_full")

reference_files <- list.files(GERM_PATH, "[.]fasta", full.names = T)

v_files <- grep(short_ref, reference_files, value = T)


if (short) {
  fr1_names <-
    read.delim(list.files(GERM_PATH, "bp[.]tsv", full.names = T),
               stringsAsFactors = F)
  fr1_names$ALLELES <- gsub(" ", "", gsub(",", "_", fr1_names$ALLELES))
  fr1_names$v_call <- paste0(fr1_names$GENE, "[*]", fr1_names$PATTERN)
  fr1_names$v_call_new <-
    paste0(fr1_names$GENE, "*", fr1_names$ALLELES)
}

germline_db_V <-
  tools::file_path_sans_ext(grep("V", v_files, value = T))
germline_db_J <-
  tools::file_path_sans_ext(grep("J", reference_files, value = T))

VGERM_PATH <-
  grep("V", v_files, value = T)
JGERM_PATH <-
  grep("J", reference_files, value = T)

###### Initial germline for genotype and IgBAST 1 ####

VGERM <- tigger::readIgFasta(VGERM_PATH)

JGERM <- tigger::readIgFasta(JGERM_PATH)


### if not light add d germ

if (chain == "IGH") {
  germline_db_D <-
    tools::file_path_sans_ext(grep("D", reference_files, value = T))
  DGERM_PATH <-
    grep("IGHD", reference_files, value = T)
  DGERM <- tigger::readIgFasta(DGERM_PATH)
  
  
  MAKEDBREPO <- paste(VGERM_PATH, DGERM_PATH, JGERM_PATH)
} else{
  ### dummy D reference
  germline_db_D <-
    tools::file_path_sans_ext(grep("IGHD", reference_files, value = T))
  MAKEDBREPO <- paste(VGERM_PATH, " ", JGERM_PATH)
}
