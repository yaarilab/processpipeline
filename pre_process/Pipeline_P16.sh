#!/bin/bash

DIRPATH=$1
ENAID=$2
SAMP=$3

cd $1
nice -19 AssemblePairs.py align -1 $2_1.fastq -2 $2_2.fastq --coord sra --rc tail --outname $3 --log $3_AP.log
nice -19 FilterSeq.py quality -s $3_assemble-pass.fastq -q 20 --outname $3 --log $3_FS.log
nice -19 MaskPrimers.py align -s $3_quality-pass.fastq -p /misc/work/avivo/BIOMED2/primers/Biomed_V_primers.fasta --maxlen 100 --mode cut --pf VPRIMER --outname $3-FWD --log $3_MPV.log
nice -19 MaskPrimers.py align -s $3-FWD_primers-pass.fastq -p /misc/work/avivo/BIOMED2/primers/Biomed_J_primers.fasta --mode cut --revpr --pf JPRIMER --outname $3-REV --log $3_MPC.log
nice -19 CollapseSeq.py -s $3-REV_primers-pass.fastq -n 20 --inner --uf JPRIMER --cf VPRIMER --act set --outname $3
nice -19 SplitSeq.py group -s $3_collapse-unique.fastq -f DUPCOUNT --num 2 --outname $3
nice -19 ParseHeaders.py table -s $3_atleast-2.fastq -f ID DUPCOUNT JPRIMER VPRIMER
nice -19 ParseLog.py -l $3_AP.log -f ID LENGTH OVERLAP ERROR PVALUE
nice -19 ParseLog.py -l $3_FS.log -f ID QUALITY
nice -19 ParseLog.py -l $3_MPV.log $3_MPC.log -f ID PRIMER ERROR
