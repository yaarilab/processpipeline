#!/bin/bash

## handles cmd options
while getopts ":l:r:c:p:s:e:ciso:" option ; do
        case "${option}" in
				l) R1_FILE=${OPTARG};;	  # R1 file 
				r) R2_FILE=${OPTARG};;	  # R2 file 
				c) SAMPLE_FILE=${OPTARG};;	  # sample file 
                s) STEP_START=${OPTARG};; # start step
                e) STEP_END=${OPTARG};;	  # end step
        esac
done


usage() { echo "Usage: $0 [-l R1.fastq] [-r R2.fastq] [-s 1] [-e 2] [-c sample_file.txt]" 1>&2; exit 1; }
if [ -z "$R1_FILE" ] || [ -z "$R2_FILE" ]  || [ -z "$SAMPLE_FILE" ]; then
    usage
fi
echo "R1=$R1_FILE,R2=$R2_FILE,SAMPLE=$SAMPLE_FILE,START=$STEP_START,END=$STEP_END" 

if [ -z "$STEP_START" ] || [ -z "$STEP_END" ] ; then
    echo "Either start step or end step not defined correctly"; exit 1;
fi



##########################################################################################
#############################          local Parameters           ##############################
##########################################################################################

VERSION=1   
echo "Running RACE300 Version "$VERSION
SUFFIX_MAIN="/home/bcrlab/eitan/scripts/RACE_300_V"$VERSION"_suffix.main"

RUN="nice -19" 
LOGDIR="/$(pwd)/logs/"
RUNLOG="/$(pwd)/${FILEDIR}/logs/Pipeline.log"    						##saved in the directory folder in a file called Pipeline.log
WORKDIR="/$(pwd)/"

#samples
LIBS=($(awk '{ print $1 }' $SAMPLE_FILE))
ORGS=($(awk '{ print $3 }' $SAMPLE_FILE))
IGORGS=($(awk '{ print $4 }' $SAMPLE_FILE))
export IGDATA=/home/bcrlab/eitan/data/igblast/
IGV_repo=$IGDATA/IGV_w_gaps/human_mouse_IGV_repo.fa						# for blast IGV [first filter step]

BCDF=Barcode_M1S_Z.fasta
COMP_ISO=FALSE
MAXCORES=24
PAIRAWK_BC=/home/bcrlab/eitan/scripts/pairawk_bc.sh                 #Pair seqs and transfer UMI between pairs 
PAIRAWK_PRCONS=/home/bcrlab/eitan/scripts/pairawk_prcons.sh	        #Pair seqs after building consensus
UMI_GROUP=/home/bcrlab/eitan/scripts/umi-v.0.4.py                   # Levinshtein edit distance = 2  (complete linkage, hierarchical)                 
MUSCLE=/private/tools/muscle3.8.31_i86linux64
USEARCH=/private/tools/usearch8.1.1825_i86linux32
PEAR=/private/tools/pear-0.9.10-linux-x86_64/bin/pear
PHIXDB=/home/bcrlab/eitan/data/other/phiX174.fasta

PRIF=/home/bcrlab/eitan/scripts/SmartNNNa.fasta
ISOPRIF=/home/bcrlab/eitan/scripts/isotypes
COMPPR=/home/bcrlab/eitan/scripts/CRegion
SUFFIX_LOG="name_suffix.log"

QUAL=20
MAXERR=0.05 # for sample barcodes
UMI_MAX_SIZE=500 # max UMI size; limits number of reads in each UMI group 
CLUSTER_ID=0.9 # cluster identity; controls sub-clustering within UMI sets
MAXDIV_M1S=0.1 # BuildConsensus (M1S)
MAXDIV_Z=0.2 #  BuildConsensus (Z) [relaxed compared to M1S due to primer diversity in isotype region]

tmpR1=tmpR1
tmpR2=tmpR2
R1_PREFIX=R1
R2_PREFIX=R2

####################################################################
### system parameters  - DO NOT CHANGE THIS     ####################
NPROC=3  # DO NOT CHANGE THIS ! may crash cluster if increased 
if [ "$MAXCORES" -gt 40 ] ; then  ## DO NOT CHANGE THIS ! may crash cluster if increased
    echo "MAXCORES exceeded maximum allowed"; exit 1;
fi
 

####################################################################
## Functions
#####################################################################
batch_FilterSeq () {
		local suffix=$1
		local tmpf=$2
		#local outf=$2
		local outl=$3
		local cores=$4
		local step=$5
	
			log=$(base=$(basename $RUNLOG);  echo $base | sed 's/log//')$step".log"
			log=$LOGDIR$log

			i=0; parr=() ; farr=()
			for file in $tmpf/*$suffix  ; do name=$(basename $file) ; sname=$(basename $name .fastq) ; $RUN  FilterSeq.py quality -s $tmpf/$name -q $QUAL --nproc $NPROC --outname $sname --log $tmpf/$sname"_quality-pass.log" >> $tmpf/$sname".Pipeline.log" & pid=`echo $! ` ; parr[$i]=`echo $pid` ; farr[$i]=$file ; ((i=i+1)) ; done 
			for ((n=0; n<$i; n++)) ; do wait ${parr[$n]} ; done
			#for ((n=0; n<$i; n++)) ; do rm ${farr[$n]} ; done  # remove unfiltered files
			cat $tmpf/*".Pipeline.log" >> $log
			rm $tmpf/*".Pipeline.log"
			cat $tmpf/*.log >> $outl			
			rm $tmpf/*log
		#fi
		
	return 0
}


calc_read_count () { 
	local fastq=$1
	local cores=$2
	line=$(head -1 $fastq) ; mac=(${line//:/ }); x=$(grep -c "^"$mac $fastq) ; eval "$3=$(($x/$cores))"
}

countPhix () {

		local seq_fq=$1 
		base=$(basename $seq_fq .fastq)
		local seq_fa=$base".fa"
		local seq_blast=$base".blastfmt6"
		
		awk '{if(NR%4==1) {printf(">%s\n",substr($0,2));} else if(NR%4==2) print;}' $seq_fq > $seq_fa
		$RUN /private/tools/ncbi-blast/bin/blastn -query $seq_fa -db $PHIXDB -task blastn -outfmt 6 -out $seq_blast -num_threads 8
		phi_count = $(awk '{if ($11 < 0.001) { print $1}}' $seq_blast | sort | uniq | wc -l )
		rm $seq_blast
		return $phi_count
		
		## select phix reads (if needed)
		#local seq_phix=$base".phix.ids"
		#local seq_clean=$base".clean.fastq"		
		#while read line; do grep -A3 --no-group-separator $line $seq_fq ; done < $seq_phix > $seq_clean  
		
}

split_and_blast () {
				
		local seq_fq=$1
		local db=$2
		local tmpf=$3
		local cores=$4
		local step=$5
	
		log=$(base=$(basename $RUNLOG);  echo $base | sed 's/log//')$step".log"
		log=$LOGDIR$log
			
		if [ ! -d $tmpf ]; then
			mkdir $tmpf
		fi
		
		#split files
		if [ ! "$(ls -A $tmpf)" ]; then  # checks if directory is empty
			fileSize=''			
			calc_read_count $seq_fq $cores fileSize
			echo "splitting read files, each file will contain" $fileSize "sequences" 
			SplitSeq.py count -s $seq_fq --outdir $tmpf -n $fileSize >> $log 
		fi
					
		# run blast
		i=0; parr=() ; farr=()
		for file in $tmpf/*  ; do 
			name=$(basename $file) ; sname=$(basename $name .fastq) ; 
			faFile=$sname".fa" 
			blastFile=$sname".blastfmt6" 
			blastError=$sname".blasterr" 
			idFile=$sname".ids" 
			awk '{if(NR%4==1) {printf(">%s\n",substr($0,2));} else if(NR%4==2) print;}' $file > $tmpf/$faFile
			
			$RUN /private/tools/ncbi-blast/bin/blastn  -query $tmpf/$faFile -db $db -task blastn -outfmt 6 -out $tmpf/$blastFile -max_target_seqs 1 -num_threads $NPROC 2>$tmpf/$blastError & pid=`echo $! ` ; 
			
			parr[$i]=`echo $pid` ; farr[$i]=$file ; ((i=i+1)) ; 
		done
		for ((n=0; n<$i; n++)) ; do wait ${parr[$n]} ; done
			
		# parse blast results 
		echo "parsing blast results"
		i=0; parr=() ; farr=()
		for file in $tmpf/*.fa  ; do 
			name=$(basename $file) ; sname=$(basename $name .fa) ; 
			blastFile=$sname".blastfmt6" 
			idFile=$sname".igv-pass.ids" 
			awk '{if ($11 < 0.001) { print $1}}' $tmpf/$blastFile | sort | uniq > $tmpf/$idFile
			hit_count=$( wc -l $tmpf/$idFile | awk '{print $1}' )
			seq_count=$( wc -l $file | awk '{print $1/4}' )
			echo $hit_count"/"$seq_count"  "$sname"  sequences match V gene for "$blastFile
		done 
		
		return 0
}
		
batch_MaskPrimers () {

		local tmpf=$1
		local primf=$2
		local outl=$3
		local mask_mode=$4
		local fsuf=$5
		local skip=$6
		local maxlen=$7
		local step=$8
		local pStart=$9
		
		
		local PROC=3

		if [ ! -d $tmpf ]; then
			echo "missing directory $tmpf , exiting"
			exit 1
		fi
		
		log=$(base=$(basename $RUNLOG);  echo $base | sed 's/log//')$step".log"
		log=$LOGDIR$log
		
		if [ "$(ls -A $tmpf)" ]; then #if directory has files
			
			i=0; parr=() 			
			if [ "$mask_mode" == "score" ]; then
				for file in $tmpf/*$fsuf  ; do name=$(basename $file) ; sname=$(basename $name .fastq) ; $RUN  MaskPrimers.py score -s $tmpf/$name -p $primf --mode cut --start $pStart --log $tmpf/$sname".mask_primers_score.log" --outname $sname --nproc $NPROC  --maxerror $MAXERR --barcode --failed >> $tmpf/$sname".Pipeline.log" & pid=`echo $! ` ; parr[$i]=`echo $pid` ; ((i=i+1)) ; done 
				for ((n=0; n<$i; n++)) ; do wait ${parr[$n]} ; done
			elif [ "$mask_mode" == "align" ]; then
				for file in $tmpf/*$fsuf  ; do name=$(basename $file) ; sname=$(basename $name .fastq)  ; $RUN  MaskPrimers.py align -s $tmpf/$name --nproc $PROC -p $primf --mode cut  --skiprc --log $tmpf/$sname".mask_primers_align.log" --outname $sname --maxerror $MAXERR --maxlen $maxlen --failed >> $tmpf/$sname".Pipeline.log" & pid=`echo $! ` ; parr[$i]=`echo $pid` ; ((i=i+1)) ; done 
				for ((n=0; n<$i; n++)) ; do wait ${parr[$n]} ; done
			else 
				echo  "Fatal Error: Cannot build command for MaskPrimers !"
				exit 1
			fi
		
		cat $tmpf/*".Pipeline.log" >> $log
		rm $tmpf/*".Pipeline.log"
		cat $tmpf/*.log >> $outl			
		rm $tmpf/*log
		
		fi
		
	return 0
}
		
run_blast () {
				
		local seq_fq=$1
		local db=$2
		local tmpf=$3
		local cores=$4
			
		i=0; parr=() ; farr=()
		for file in $tmpf/*.fa  ; do 
			name=$(basename $file) ; sname=$(basename $name .fastq) ; 
			blastFile=$sname".mouse.blastfmt6" 
			$RUN /private/tools/ncbi-blast/bin/blastn -warnings -query $file -db $db -task blastn -outfmt 6 -out $tmpf/$blastFile -max_target_seqs 1 -num_threads $NPROC & pid=`echo $! ` ; 
			parr[$i]=`echo $pid` ; farr[$i]=$file ; ((i=i+1)) ; 
		done
		
		for ((n=0; n<$i; n++)) ; do wait ${parr[$n]} ; done
			
		return 0
}
		


##########################################################################################
################################          START           ################################
##########################################################################################

STEP=$STEP_START

if [ ! -d $LOGDIR ]; then
		mkdir $LOGDIR
	fi
	
###############################          fastqc           ###############################
	#fastqc - FastQC aims to provide a simple way to do some quality control checks on raw sequence data coming from high throughput sequencing pipelines.
	#./paaaath/FastQC/fastqc
	
	if [ $[$STEP] -eq 1 ]; then
		echo "STEP $STEP: Fastqc   $(date +'%H:%M %D')"
		/private/tools/FastQC/fastqc $R1_FILE 
		/private/tools/FastQC/fastqc $R2_FILE 
		STEP=$((STEP+1)); if [ $[$STEP] -gt $STEP_END ]; then exit; fi
	fi
	
	################ split files and run blast on sequences (keeping hits to IGV) #################
	
	if [ $[$STEP] -eq 2 ]; then
		
		
		echo "STEP $STEP: Split and Blast " $R1_FILE "   $(date +'%H:%M %D')"
		split_and_blast $R1_FILE $IGV_repo $tmpR1 $MAXCORES $STEP
		
		echo "STEP $STEP: Split and Blast " $R2_FILE "   $(date +'%H:%M %D')"
		split_and_blast $R2_FILE $IGV_repo $tmpR2 $MAXCORES $STEP
			
		echo "STEP $STEP: Split and Blast , pair sequenecs,   $(date +'%H:%M %D')"
		# keep ids that have a blast ID in either R1 or R2
		for file in $tmpR1/*.fastq  ; do 
			R1_name=$(basename $file) ; R1_sname=$(basename $R1_name .fastq) ; 
			R2_name=$(echo $R1_name | sed 's/R1/R2/') ; R2_sname=$(echo $R1_sname | sed 's/R1/R2/') ;
			R1_idFile=$R1_sname".igv-pass.ids" 
			R2_idFile=$R2_sname".igv-pass.ids" 
			R1_combIDFile=$R1_sname".igv-pass.R1_R2.ids" 
			R2_combIDFile=$R2_sname".igv-pass.R1_R2.ids" 
			cat $tmpR1/$R1_idFile $tmpR2/$R2_idFile > $tmpR1/$R1_combIDFile
			cat $tmpR1/$R1_idFile $tmpR2/$R2_idFile > $tmpR2/$R2_combIDFile
			
			# for R1
			# get fastq sequenecs blast hit
			awk 'NR==FNR {id["@"$1]=1; } NR!=FNR && FNR%4==1 {split($0,b," "); if (id[b[1]]==1) {print $0; flag=1}} NR!=FNR && FNR%4!=1 {if(flag==1){print $0; if (FNR%4==0){flag=0}}}' $tmpR1/$R1_combIDFile $tmpR1/$R1_name  > $tmpR1/$R1_sname".igv-pass.fastq"
			
			# those without blast hit
			awk 'NR==FNR {id["@"$1]=1; } NR!=FNR && FNR%4==1 {split($0,b," "); if (id[b[1]]!=1) {print $0; flag=1}} NR!=FNR && FNR%4!=1 {if(flag==1){print $0; if (FNR%4==0){flag=0}}}' $tmpR1/$R1_combIDFile $tmpR1/$R1_name > $tmpR1/$R1_sname".igv-fail.fastq"
	
			# for R2
			# get fastq sequenecs blast hit
			awk 'NR==FNR {id["@"$1]=1;} NR!=FNR && FNR%4==1 {split($0,b," "); if (id[b[1]]==1) {print $0; flag=1}} NR!=FNR && FNR%4!=1 {if(flag==1){print $0; if (FNR%4==0){flag=0}}}' $tmpR2/$R2_combIDFile $tmpR2/$R2_name  > $tmpR2/$R2_sname".igv-pass.fastq"
			
			# those without blast hit
			awk 'NR==FNR {id["@"$1]=1; } NR!=FNR && FNR%4==1 {split($0,b," "); if (id[b[1]]!=1) {print $0; flag=1}} NR!=FNR && FNR%4!=1 {if(flag==1){print $0; if (FNR%4==0){flag=0}}}' $tmpR2/$R2_combIDFile $tmpR2/$R2_name  >  $tmpR2/$R2_sname".igv-fail.fastq"
			
		done
		
		STEP=$((STEP+1)); if [ $[$STEP] -gt $STEP_END ]; then exit; fi	
#		if [ $[$STEP+1] -gt $STEP_END ]; then cd ../ ; continue; else STEP=$((STEP+1)) ; fi  
	fi
	
	###############################		 Filter Quality 	   ##############################

	if [ $[$STEP] -eq 3 ]; then
		echo "STEP $STEP: FilterSeq filter   $(date +'%H:%M %D')"
		
		suffix=".igv-pass.fastq"
		R1log=$LOGDIR"QualityLogR1_table.tab"
		batch_FilterSeq $suffix $tmpR1 $R1log $MAXCORES $STEP

		suffix=".igv-pass.fastq"
		R2log=$LOGDIR"QualityLogR2_table.tab"
		batch_FilterSeq $suffix $tmpR2 $R2log $MAXCORES $STEP

		STEP=$((STEP+1)); if [ $[$STEP] -gt $STEP_END ]; then exit; fi
	fi
	
################ Find M1S/Z-sample barcodes matches  #####################

	if [ $[$STEP] -eq 4 ]; then
		echo "STEP $STEP-a: MaskPrimers align R1      $(date +'%H:%M %D')"

		MPMode="align"
		skipRC="TRUE"  # check only Forward primer sequences
		maxLen=50   # check only presence at the beginning of the read (if at the end then fragment too short)
		InFileSuffix="quality-pass.fastq"
		R1log=$LOGDIR"PrimerILogR1_table.tab"
		batch_MaskPrimers $tmpR1 $BCDF $R1log $MPMode $InFileSuffix $skipRC $maxLen $STEP

		echo "STEP $STEP-b: MaskPrimers align R2      $(date +'%H:%M %D')"

		R2log=$LOGDIR"PrimerILogR2_table.tab"
		batch_MaskPrimers $tmpR2 $BCDF $R2log $MPMode $InFileSuffix $skipRC $maxLen $STEP

		## separate between M1S and Z sequences
		InFileSuffix="primers-pass.fastq"
		for file in $tmpR1/*$InFileSuffix  ; do base=$(echo $file | sed 's/\.fastq//') ; $RUN grep -A 3 --no-group-separator  "\-M1S"   $file > $base"_M1S.fastq" ; done
		for file in $tmpR1/*$InFileSuffix  ; do base=$(echo $file | sed 's/\.fastq//') ; $RUN grep -A 3 --no-group-separator  "\-Z"   $file > $base"_Z.fastq" ; done
		for file in $tmpR2/*$InFileSuffix  ; do base=$(echo $file | sed 's/\.fastq//') ; $RUN grep -A 3 --no-group-separator  "\-M1S"   $file > $base"_M1S.fastq" ; done
		for file in $tmpR2/*$InFileSuffix  ; do base=$(echo $file | sed 's/\.fastq//') ; $RUN grep -A 3 --no-group-separator  "\-Z"   $file > $base"_Z.fastq" ; done
	
		STEP=$((STEP+1)); if [ $[$STEP] -gt $STEP_END ]; then exit; fi
	fi
	################ Identify smartNNN primer and extract UMI at a fixed posotion #################
	
	if [ $[$STEP] -eq 5 ]; then
		echo "STEP $STEP-a: MaskPrimers score R1      $(date +'%H:%M %D')"

		MPMode=score
		skipRC="TRUE" 
		maxLen=50 
		pStart=16
		MAXERR=0.3 # short primer sequence, more relaxed
		InFileSuffix="primers-pass_M1S.fastq"
		R1log=$LOGDIR"PrimerLogR1_table.tab"

		batch_MaskPrimers $tmpR1 $PRIF $R1log $MPMode $InFileSuffix $skipRC $maxLen $STEP $pStart 

		echo "STEP $STEP-b: MaskPrimers score R2      $(date +'%H:%M %D')"

		R2log=$LOGDIR"PrimerLogR2_table.tab"
		batch_MaskPrimers $tmpR2 $PRIF $R2log $MPMode $InFileSuffix $skipRC $maxLen $STEP $pStart 
		MAXERR=0.2

		$RUN  ParseLog.py -l $R1log -f BARCODE PRIMER ERROR
		$RUN  ParseLog.py -l $R2log -f BARCODE PRIMER ERROR
		
		STEP=$((STEP+1)); if [ $[$STEP] -gt $STEP_END ]; then exit; fi
	fi
	

	
################ Pair seqs and transfer UMI between pairs #################


	if [ $[$STEP] -eq 6 ]; then
		echo "STEP $STEP: PairAwk      $(date +'%H:%M %D')"
		InFileSuffix="_M1S_primers-pass.fastq"
		# without isotypes:
		for file in $tmpR1/*$InFileSuffix  ; do base=$(basename $file) ; R2=$(echo $base | sed 's/M1S_primers-pass/Z/' | sed 's/R1/R2/') ; $RUN  sh $PAIRAWK_BC $file $tmpR2/$R2  ; done 
		for file in $tmpR2/*$InFileSuffix  ; do base=$(basename $file) ; R1=$(echo $base | sed 's/M1S_primers-pass/Z/' | sed 's/R2/R1/') ; $RUN  sh $PAIRAWK_BC $file $tmpR1/$R1  ; done 
		cat $tmpR1/*M1S_primers-pass_pair-pass.fastq $tmpR2/*M1S_primers-pass_pair-pass.fastq > M1S_pair_pass.fastq  # R1+R2 protocol
		cat $tmpR2/*Z_pair-pass.fastq $tmpR1/*Z_pair-pass.fastq > Z_pair_pass.fastq  				   				 # R1+R2 protocol
		
		STEP=$((STEP+1)); if [ $[$STEP] -gt $STEP_END ]; then exit; fi
	fi
	
	
##
AMAX=$((${#LIBS[@]}-1))

for i in `seq 0 $AMAX` ; do
	LIB=${LIBS[$i]}
	ORG=${ORGS[$i]}
	IGORG=${IGORGS[$i]}
	
	echo "LIB=$LIB, ORG=$ORG, IGORG=$IGORG"
	
	if [ ! -d $LIB ]; then
		mkdir $LIB
	fi
	
	cd $LIB
	echo "pwd=$(pwd)"
	
	if [ ! -s  M1S_pair_pass.$LIB.fastq ]; then
		$RUN grep -A 3 --no-group-separator "="$LIB"-" ../M1S_pair_pass.fastq > M1S_pair_pass.$LIB.fastq  # only the first barcode can contain $LIB, this is important to avoid isotype cross match 
	fi
	
	if [ ! -s  Z_pair_pass.$LIB.fastq ]; then
		$RUN grep -A 3 --no-group-separator "="$LIB"-" ../Z_pair_pass.fastq > Z_pair_pass.$LIB.fastq 
	fi
	
	LIB_START_STEP=$STEP
	
	################ Identify isotypes #################
	
	if [ $[$STEP] -eq 7 ]; then
		
		if [ "$COMP_ISO" = "TRUE" ]; then
		
				echo "STEP $STEP-a: Compuatational Isotype MaskPrimers align   $(date +'%H:%M %D')"

				MPMode="align"
				skipRC="TRUE"  # check only Forward primer sequences
				MAXERR=0.2 # stringent about isomers selection
				maxLen=50   # check only presence at the beginning of the read (if at the end then fragment too short)
				LCOMPPR=$COMPPR"."$ORG".fasta"
				
				log=$(base=$(basename $RUNLOG);  echo $base | sed 's/log//')$STEP".log"
				log=$LOGDIR$log
				sname=Z_pair_pass.$LIB
				isotypeLog=$LOGDIR$LIB".Comp.Isotype.log"
				
				$RUN MaskPrimers.py align -s Z_pair_pass.$LIB.fastq  --nproc $MAXCORES -p $LCOMPPR --mode cut  --skiprc --log $isotypeLog --outname $sname --maxerror $MAXERR --maxlen $maxLen --failed >> $log & pid=`echo $! `
				wait $pid
				
				SUFFIX="_primers-pass.fastq"
				echo $SUFFIX > $SUFFIX_LOG
				
				$RUN  ParseLog.py -l $isotypeLog -f BARCODE PRIMER ERROR  
				
				#if [ $[$STEP+1] -gt $STEP_END ]; then cd ../ ; continue; else STEP=$((STEP+1)) ; fi  
		else
	
				echo "STEP $STEP: Isotype MaskPrimers align      $(date +'%H:%M %D')"

				MPMode=align
				#MPMode=score
				#pStart=0
				skipRC="TRUE" 
				maxLen=50 
				MAXERR=0.2 # stringent about isomers selection
				LISOPRIF=$ISOPRIF"."$ORG".fasta"
				
				log=$(base=$(basename $RUNLOG);  echo $base | sed 's/log//')$STEP".log"
				log=$LOGDIR$log
				sname=Z_pair_pass.$LIB
				isotypeLog=$LOGDIR$LIB".Isotype.log"
				
				$RUN MaskPrimers.py align -s Z_pair_pass.$LIB.fastq  --nproc $MAXCORES -p $LISOPRIF --mode cut  --skiprc --log $isotypeLog --outname $sname --maxerror $MAXERR --maxlen $maxLen --failed >> $log & pid=`echo $! `
				wait $pid
				
				SUFFIX="_primers-pass.fastq"
				echo $SUFFIX > $SUFFIX_LOG
				
				$RUN  ParseLog.py -l $isotypeLog -f BARCODE PRIMER ERROR  
								
				fi

				## merge header barcode and primer fields
				cp Z_pair_pass.$LIB$SUFFIX tmp 
				awk 'NR%4==1 {split($0,a,"|"); split (a[3],bc,"="); split(a[5],isoType,"="); print(a[1]"|"a[2]"|BARCODE="bc[2]","isoType[2]"|"a[4]);} NR%4!=1 {print $0}' tmp > Z_pair_pass.$LIB$SUFFIX
				
				## pair awk between M1S and Z, so isotype information will be transfered
				awk 'NR==FNR && NR%4==1 {split($0,a,"|"); split(a[1],b,"/"); id=b[1]; z[id]=1 ; split(a[3],c,"BARCODE="); bc[id]=c[2];} NR!=FNR && FNR%4==1 {split($0,a,"|"); split(a[1],b,"/"); id=b[1]; if (z[id]==1) {header=a[1]"|"a[2]"|BARCODE="bc[id]"|"a[4]; print (header);}} NR!=FNR && FNR%4!=1 {if (z[id]==1) print}' Z_pair_pass.$LIB$SUFFIX  M1S_pair_pass.$LIB.fastq > M1S_pair_pass.$LIB$SUFFIX
				
				
				if [ $[$STEP+1] -gt $STEP_END ]; then cd ../ ; continue; else STEP=$((STEP+1)) ; fi		
				
	fi
	
		
	################ Cluster sequences #################
	
	if [ $[$STEP] -eq 8 ]; then
		
		SUFFIX=$(awk -v x="$STEP" '$1==x {print $2}' $SUFFIX_MAIN)
		echo "SUFFIX="$SUFFIX
		log=$(base=$(basename $RUNLOG);  echo $base | sed 's/log//')$STEP".log"
		log=$LOGDIR$log
		
		## Group umis with small edit distance 
		echo "STEP $STEP: Grouping Umis $LIB     $(date +'%H:%M %D')"  
				
		######### getting UMI list from the fastq file
		awk '{if (NR%4==1) {split($0,a,"UMI="); print a[2]}}' M1S_pair_pass.$LIB$SUFFIX > M1S_pair_pass.$LIB.umi
		
		#########group UMI and rename UMI in the original fastq file
		python $UMI_GROUP -i M1S_pair_pass.$LIB.umi -o M1S_pair_pass.$LIB.umi.convert # generate umi conversion table
		NEW_SUFFIX=$(echo $SUFFIX | sed 's/.fastq//')"_group-pass.fastq"
		
		awk -F"\t" 'NR==FNR && NR>1 {umi[$1]=$2;} NR!=FNR {if(/UMI=/){split($0,a,"UMI="); print a[1]"UMI="umi[a[2]];} else {print}}' M1S_pair_pass.$LIB.umi.convert M1S_pair_pass.$LIB$SUFFIX > M1S_pair_pass.$LIB$NEW_SUFFIX
		awk -F"\t" 'NR==FNR && NR>1 {umi[$1]=$2;} NR!=FNR {if(/UMI=/){split($0,a,"UMI="); print a[1]"UMI="umi[a[2]];} else {print}}' M1S_pair_pass.$LIB.umi.convert Z_pair_pass.$LIB$SUFFIX > Z_pair_pass.$LIB$NEW_SUFFIX 
		
		SUFFIX=$NEW_SUFFIX
		echo $SUFFIX >> $SUFFIX_LOG
		
		echo "STEP $STEP: Clustering sequences $LIB     $(date +'%H:%M %D')"
        ###### clustering sequences (only M1S)

        echo "CLUSTER_ID="$CLUSTER_ID", performing UMI sub clustering !"

        clusterSetsLog=$LOGDIR$LIB".M1S.clusterSets.log"
        ClusterSets.py set -s M1S_pair_pass.$LIB$SUFFIX --failed --log $clusterSetsLog --nproc 24 -f UMI --exec $USEARCH -k CLID --id $CLUSTER_ID >> $log & pid=`echo $! `
		wait $pid

		PREV_SUFFIX=$SUFFIX
        SUFFIX=$(echo $SUFFIX | sed 's/.fastq//')"_cluster-pass.fastq"
		TMP1_SUFFIX=$(echo $SUFFIX | sed 's/.fastq//')".ori.fastq"
        TMP2_SUFFIX=$(echo $SUFFIX | sed 's/.fastq//')".UMI_TO_CLUSTER.txt"
			
		# take the biggest cluster of each UMI 
		mv M1S_pair_pass.$LIB$SUFFIX M1S_pair_pass.$LIB$TMP1_SUFFIX
			
		awk 'BEGIN{LARGEST_CLID="";LARGEST_SIZE=0;print "UMI\tLARGEST_CLID\tCLID_SIZE";} 
			 NR%4==1 {split($0,a,"|"); split(a[4],b,"UMI="); UMI=b[2]; split(a[5],c,"CLID="); CLID=c[2];  if(UMI==PREV_UMI || NR==1) {ARR[UMI"_"CLID]+=1; if(ARR[UMI"_"CLID]>LARGEST_SIZE) {LARGEST_SIZE=ARR[UMI"_"CLID]; LARGEST_CLID=CLID} ; if (NR==1) {PREV_UMI=UMI}; next } else { print PREV_UMI"\t"LARGEST_CLID"\t"LARGEST_SIZE; ARR[UMI"_"CLID]+=1; LARGEST_CLID=CLID;LARGEST_SIZE=ARR[UMI"_"CLID]; PREV_UMI=UMI; next }} 
			 END { print PREV_UMI"\t"LARGEST_CLID"\t"LARGEST_SIZE;} ' \
			 M1S_pair_pass.$LIB$TMP1_SUFFIX  > M1S_pair_pass.$LIB$TMP2_SUFFIX
						
		awk 'NR==FNR && NR>1 {UMI=$1;CLID=$2;ID[UMI]=CLID} 
			 NR!=FNR && FNR%4==1 {split($0,a,"|"); split(a[4],b,"UMI="); UMI=b[2]; split(a[5],c,"CLID="); CLID=c[2];  if(CLID==ID[UMI]){flag=1; print}else{flag=0}}
			 NR!=FNR && FNR%4!=1 && flag==1 {print}' \
			 M1S_pair_pass.$LIB$TMP2_SUFFIX M1S_pair_pass.$LIB$TMP1_SUFFIX > M1S_pair_pass.$LIB$SUFFIX
				 
		# combine cluster and UMI fields
        sed -i 's/|CLID=/-/' M1S_pair_pass.$LIB$SUFFIX

        ## pair awk between M1S and Z, so cluster-UMI information will be transfered
        awk 'NR==FNR && NR%4==1 {split($0,a,"|"); id=a[1]; split(a[4],b,"UMI="); umi=b[2]; z[id]=umi;} 
			 NR!=FNR && FNR%4==1 {split($0,a,"|"); id=a[1]; header=a[1]"|"a[2]"|"a[3]"|UMI="z[id]; print (header);} 
			 NR!=FNR && FNR%4!=1 {print}' \
			 M1S_pair_pass.$LIB$SUFFIX Z_pair_pass.$LIB$PREV_SUFFIX > Z_pair_pass.$LIB$SUFFIX

		if [ $[$STEP+1] -gt $STEP_END ]; then cd ../ ; continue; else STEP=$((STEP+1)) ; fi    
	fi
	
	################ Align UID sets ################
	
	if [ $[$STEP] -eq 9 ]; then
	
		log=$(base=$(basename $RUNLOG);  echo $base | sed 's/log//')$STEP".log"
		log=$LOGDIR$log
		
		SUFFIX=$(awk -v x="$STEP" '$1==x {print $2}' $SUFFIX_MAIN)
		
		echo "STEP $STEP: Filtering UID sets larger then $UMI_MAX_SIZE , $LIB   $(date +'%H:%M %D')" 
		NEW_SUFFIX=$(echo $SUFFIX | sed 's/.fastq//')"_size-pass.fastq"
		awk -v UMI_MAX="$UMI_MAX_SIZE" -v out1="M1S_pair_pass.$LIB$NEW_SUFFIX" -v out2="Z_pair_pass.$LIB$NEW_SUFFIX" \
			'NR%4==1 && NR==FNR {split($0,a,"|"); split(a[4],b,"UMI="); umi[b[2]]+=1; if (umi[b[2]]<=UMI_MAX){flag=1; read[a[1]]=1; print > out1 } else {flag=0}} 
			 NR%4!=1 && NR==FNR { if (flag==1) print > out1 } 
			 NR%4==1 && NR!=FNR { split($0,a,"|"); if (read[a[1]]==1) {flag=1; print > out2 } else {flag=0}} 
			 NR%4!=1 && NR!=FNR {if (flag==1) print > out2 } ' \
			 M1S_pair_pass.$LIB$SUFFIX Z_pair_pass.$LIB$SUFFIX   
			 
		SUFFIX=$NEW_SUFFIX
		echo $SUFFIX >> $SUFFIX_LOG
		
		echo "STEP $STEP: AlignSets $LIB  $(date +'%H:%M %D')" 
		# align sets
		$RUN  AlignSets.py muscle -s M1S_pair_pass.$LIB$SUFFIX  --exec $MUSCLE --log $LOGDIR"AlignSets.M1S."$LIB".muscle.log"  --nproc $MAXCORES --bf UMI --div   >> $log & pid=`echo $! `
		wait $pid
		$RUN  AlignSets.py muscle -s Z_pair_pass.$LIB$SUFFIX --exec $MUSCLE --log $LOGDIR"AlignSets.Z."$LIB".muscle.log"  --nproc $MAXCORES --bf UMI --div   >> $log & pid=`echo $! `
		wait $pid
		
		SUFFIX=$(echo $SUFFIX | sed 's/.fastq//')"_align-pass.fastq"
		echo $SUFFIX >> $SUFFIX_LOG

		# replace long gaps (at least 25) with Ns before build consensus (where Ns are ignored in diversity calculation)
		mv  "M1S_pair_pass."$LIB$SUFFIX tmp 
		perl -pe 's/(-{25,})/"N" x length($1)/e' tmp > M1S_pair_pass.$LIB$SUFFIX
		mv  "Z_pair_pass."$LIB$SUFFIX tmp   
		perl -pe 's/(-{25,})/"N" x length($1)/e' tmp > Z_pair_pass.$LIB$SUFFIX
		rm tmp
		
		if [ $[$STEP+1] -gt $STEP_END ]; then cd ../ ; continue; else STEP=$((STEP+1));  fi
	fi
	
	
	############### Build Consensus #################
	if [ $[$STEP] -eq 10 ]; then	
		echo "STEP $STEP: BuildConsensus  $LIB      $(date +'%H:%M %D')" 
		log=$(base=$(basename $RUNLOG);  echo $base | sed 's/log//')$STEP".log"
		log=$LOGDIR$log
		
		SUFFIX=$(awk -v x="$STEP" '$1==x {print $2}' $SUFFIX_MAIN)
		
		$RUN BuildConsensus.py -s M1S_pair_pass.$LIB$SUFFIX  --bf UMI --cf BARCODE --act set --log $LOGDIR"BuildConsensus.M1S."$LIB".log" --nproc $MAXCORES -q 0 --freq 0.6 --failed --pf BARCODE --maxdiv $MAXDIV_M1S --maxgap 0.2 --prcons 0.4 >> $log & pid=`echo $! `
		wait $pid
		
		$RUN BuildConsensus.py -s Z_pair_pass.$LIB$SUFFIX --bf UMI --cf BARCODE --act set --log $LOGDIR"BuildConsensus.Z."$LIB".log" --nproc $MAXCORES -q 0 --freq 0.6 --failed --pf BARCODE --maxdiv $MAXDIV_Z --maxgap 0.2 --prcons 0.4 >> $log  & pid=`echo $! `
		wait $pid
		
		$RUN  ParseLog.py -l $LOGDIR"BuildConsensus.M1S."$LIB".log" -f BARCODE SEQCOUNT CONSCOUNT PRIMER PRCOUNT PRCONS PRFREQ DIVERSITY	   #OK
		cp $LOGDIR"BuildConsensus.M1S."$LIB"_table.tab" ./
		$RUN  ParseLog.py -l $LOGDIR"BuildConsensus.Z."$LIB".log" -f BARCODE SEQCOUNT CONSCOUNT PRIMER PRCOUNT PRCONS PRFREQ DIVERSITY	   #OK
		cp $LOGDIR"BuildConsensus.Z."$LIB"_table.tab" ./
		
		SUFFIX=$(echo $SUFFIX | sed 's/.fastq//')"_consensus-pass.fastq"
		echo $SUFFIX >> $SUFFIX_LOG
		
		if [ $[$STEP+1] -gt $STEP_END ]; then cd ../ ; continue; else STEP=$((STEP+1));  fi
	fi
	
	
	################# Pair sequences #################
	if [ $[$STEP] -eq 11 ]; then
		
		SUFFIX=$(awk -v x="$STEP" '$1==x {print $2}' $SUFFIX_MAIN)
		
		# Filterseq trimqual consensus sequences (to avoid quality reduction at end of sequence as a results of high N count)
		#$RUN FilterSeq.py trimqual -s M1S_pair_pass.$LIB$SUFFIX --log $LOGDIR"TrimQual.M1S."$LIB".log" --nproc $MAXCORES -q 15 
		#$RUN FilterSeq.py trimqual -s Z_pair_pass.$LIB$SUFFIX --log $LOGDIR"TrimQual.Z."$LIB".log" --nproc $MAXCORES -q 15
	
		#SUFFIX=$(echo $SUFFIX | sed 's/.fastq//')"_trimqual-pass.fastq"
		#echo $SUFFIX >> $SUFFIX_LOG
		
		echo "STEP $STEP: PairSequences,    $LIB      $(date +'%H:%M %D')" 
		$RUN sh $PAIRAWK_PRCONS M1S_pair_pass.$LIB$SUFFIX Z_pair_pass.$LIB$SUFFIX  # takes conscount from M1S only (clustering is based on M1S)
		
		SUFFIX=$(echo $SUFFIX | sed 's/.fastq//')"_pair-pass.fastq"
		echo $SUFFIX >> $SUFFIX_LOG
		
		if [ $[$STEP+1] -gt $STEP_END ]; then cd ../ ; continue; else STEP=$((STEP+1)); fi
	fi
	
	
	################ Assembles paired-end reads into a single sequence ##################
	if [ $[$STEP] -eq 12 ]; then
	
		SUFFIX=$(awk -v x="$STEP" '$1==x {print $2}' $SUFFIX_MAIN)
		echo "STEP $STEP: AssemblePairs,  $LIB     $(date +'%H:%M %D')" 
		
		log=$(base=$(basename $RUNLOG);  echo $base | sed 's/log//')$STEP".log"
		log=$LOGDIR$log
		
		# Assemble align
		$RUN AssemblePairs.py align -1 M1S_pair_pass.$LIB$SUFFIX -2 Z_pair_pass.$LIB$SUFFIX --failed --log $LOGDIR"AssemblePairs-align."$LIB".log" --nproc $MAXCORES --minlen 8 --alpha 0.00001 --maxerror 0.3 --rc "tail" --scanrev --fasta --1f PRCONS PRFREQ --2f CONSCOUNT BARCODE >> $log & pid=`echo $! `
		wait $pid
		
		$RUN  ParseLog.py -l $LOGDIR"AssemblePairs-align."$LIB".log" -f ID LENGTH OVERLAP ERROR PVALUE FIELDS1 FIELDS2  # CONSCOUNT BARCODE
		
		FAIL_SUFFIX=$(echo $SUFFIX | sed 's/.fastq//')"_assemble-fail.fasta"
		SUFFIX=$(echo $SUFFIX | sed 's/.fastq//')"_assemble-pass.fasta"
		echo $SUFFIX >> $SUFFIX_LOG
			
		# assemble reference
		REG=$IGDATA/$IGORG"_w_gaps"/$ORG"_gl_IG*V_F+ORF+in-frame_P_w_gaps.fasta"
		for file in $REG ; do REF_DB=$file ; done 		# reference only by V gene
		echo "REF_DB=$REF_DB"

		$RUN /private/tools/presto-0.5.3/bin/AssemblePairs.py reference -1 M1S_pair_pass.$LIB$FAIL_SUFFIX -2 Z_pair_pass.$LIB$FAIL_SUFFIX --1f PRCONS PRFREQ --2f CONSCOUNT BARCODE --failed --exec $USEARCH -r $REF_DB --log $LOGDIR"AssemblePairs-reference."$LIB".log" --nproc $MAXCORES --fasta --rc "tail" >> $log & pid=`echo $! `
		wait $pid

		$RUN  ParseLog.py -l $LOGDIR"AssemblePairs-reference."$LIB".log" -f ID REFID LENGTH OVERLAP GAP EVALUE1 EVALUE2 IDENTITY FIELDS1 FIELDS2 

		## combine assembled 
		TMP_SUFFIX=$(echo $FAIL_SUFFIX | sed 's/.fasta//')"_assemble-pass.fasta"
		cat M1S_pair_pass.$LIB$SUFFIX M1S_pair_pass.$LIB$TMP_SUFFIX > "$LIB".assemble-pass.fasta

		SUFFIX=".assemble-pass.fasta"
		echo $SUFFIX >> $SUFFIX_LOG
		
		if [ $[$STEP+1] -gt $STEP_END ]; then cd ../ ; continue; else STEP=$((STEP+1));  fi
	fi
	
	
	################# Runs igblast 
	if [ $[$STEP] -eq 13 ]; then
	
		echo "STEP $STEP: IgBlast, $LIB      $(date +'%H:%M %D')"
		SUFFIX=$(awk -v x="$STEP" '$1==x {print $2}' $SUFFIX_MAIN)
		
		if [[ $IGORG == *"IGH" ]]; then IGTYPE="IGH"; elif [[ $IGORG == *"IGL" ]]; then IGTYPE="IGL"; fi
		echo "IGTYPE=$IGTYPE"
		
		OUTFMT="7 std qseq sseq btop" # compatible with MakeDB
		#OUTFMT="7 qseqid qgi qacc qaccver qlen sseqid sallseqid sgi sallgi sacc saccver sallacc slen qstart qend sstart send qseq sseq evalue bitscore score length pident nident mismatch positive gapopen gaps ppos frames qframe sframe btop" # compatible with vdjml igblast_parse.py
						
		$RUN /private/tools/ncbi-igblast-1.7.0/bin/igblastn -query $LIB.assemble-pass.fasta -germline_db_V $IGDATA/$IGORG"_w_gaps/"$ORG"_gl_"$IGTYPE"V_F+ORF+in-frame_P_w_gaps" -germline_db_J $IGDATA/$IGORG"_w_gaps/"$ORG"_gl_"$IGTYPE"J_F+ORF+in-frame_P" -germline_db_D $IGDATA/$IGORG"_w_gaps/"$ORG"_gl_"$IGTYPE"D_F+ORF+in-frame_P" -out $LIB.assemble-pass.igblast.out  -outfmt "$OUTFMT" -organism $ORG -num_threads 4 -domain_system imgt -show_translation -auxiliary_data $IGDATA/optional_file/"$ORG"_gl.aux &
		
		STEP=$LIB_START_STEP 
		
	fi
	
	cd ../
	
	
done 


