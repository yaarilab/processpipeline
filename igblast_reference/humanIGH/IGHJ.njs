{
  "version": "1.2",
  "dbname": "IGHJ",
  "dbtype": "Nucleotide",
  "db-version": 5,
  "description": "IGHJ.fasta",
  "number-of-letters": 701,
  "number-of-sequences": 13,
  "last-updated": "2023-06-09T12:57:00",
  "number-of-volumes": 1,
  "bytes-total": 50728,
  "bytes-to-cache": 432,
  "files": [
    "IGHJ.ndb",
    "IGHJ.nhr",
    "IGHJ.nin",
    "IGHJ.nog",
    "IGHJ.nos",
    "IGHJ.not",
    "IGHJ.nsq",
    "IGHJ.ntf",
    "IGHJ.nto"
  ]
}
