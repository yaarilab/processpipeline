{
  "version": "1.2",
  "dbname": "IGHD",
  "dbtype": "Nucleotide",
  "db-version": 5,
  "description": "IGHD.fasta",
  "number-of-letters": 1070,
  "number-of-sequences": 44,
  "last-updated": "2023-06-09T12:57:00",
  "number-of-volumes": 1,
  "bytes-total": 54150,
  "bytes-to-cache": 912,
  "files": [
    "IGHD.ndb",
    "IGHD.nhr",
    "IGHD.nin",
    "IGHD.nog",
    "IGHD.nos",
    "IGHD.not",
    "IGHD.nsq",
    "IGHD.ntf",
    "IGHD.nto"
  ]
}
