VERSION=devel
DOCKERHUB_USERNAME=peresay
DOCKERHUB_IMAGE=base
echo ${VERSION}
IMAGE="$DOCKERHUB_USERNAME/$DOCKERHUB_IMAGE"
docker build --build-arg CACHEBUST=$(date +%s) -t ${IMAGE}:${VERSION} .
#  --no-cache
docker tag ${IMAGE}:devel ${IMAGE}:${VERSION}
docker tag ${IMAGE}:devel ${IMAGE}:latest
docker push ${IMAGE}:latest
