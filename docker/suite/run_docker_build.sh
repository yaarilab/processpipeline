#VERSION=devel
#DOCKERHUB_USERNAME=peresay
#DOCKERHUB_IMAGE=base
#echo ${VERSION}
#IMAGE="$DOCKERHUB_USERNAME/$DOCKERHUB_IMAGE"
#docker build --no-cache -t ${IMAGE}:devel .
#docker tag ${IMAGE}:devel
#docker push ${IMAGE}
cd ../../
git add .
git commit -m "docker update"
git push
VERSION=$(git rev-parse HEAD | cut -c 1-8)
cd docker/suite
DOCKERHUB_USERNAME=peresay
DOCKERHUB_IMAGE=suite
echo ${VERSION}
IMAGE="$DOCKERHUB_USERNAME/$DOCKERHUB_IMAGE"
docker build --no-cache -t ${IMAGE}:${VERSION} .
docker tag ${IMAGE}:${VERSION} ${IMAGE}:latest
docker push ${IMAGE}
