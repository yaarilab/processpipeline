# VDJbase Pipeline

The geno-haplo processing pipeline for [VDJbase](http://ar.lees.org.uk/digby/). 

The pipeline contain four main scripts:

* [vdjbase_pipeline.R](https://bitbucket.org/yaarilab/processpipeline/src/master/scripts/vdjbase_pipeline.R)          pipelien script for cmd run
* [constant_parameters_vdjbase.R](https://bitbucket.org/yaarilab/processpipeline/src/master/scripts/constant_parameters_vdjbase.R)   constant parameters of the pipeline
* [functions_tigger.R](https://bitbucket.org/yaarilab/processpipeline/src/master/scripts/functions_tigger.R)          custom tigger functions
* [collapse_seq.py](https://bitbucket.org/yaarilab/processpipeline/src/master/scripts/collapse_seq.py)              collapse sequences script

# vdjbase-pipeline overview

The vdjbase-pipeline contains 5 sections:

1. Initial V(D)J alignment with [IMGT reference set](http://www.imgt.org/vquest/refseqh.html), using [IgBlast](https://ncbi.github.io/igblast/) and [MakeDb](https://changeo.readthedocs.io/en/latest/tools/MakeDb.html#makedb-img) from change-o and a [custom script](https://bitbucket.org/yaarilab/processpipeline/src/master/collapse_seq.py) for collapsing similar sequences.

2. Gene sampling and clonal inference using [DefineClones](https://changeo.readthedocs.io/en/stable/tools/DefineClones.html) from change-o. In this step it is possible to sample each gene for a representation of the repertoire. In VDJbase, for each V gene only 500 sequences (with D and J assignments) are sampled, and from these sequences, clones are inferred and one sequence per clone is taken for the downstream analysis.

3. Novel alleles inference using [TIgGER findNovelAlleles](http://tigger.readthedocs.io/) with custom gene 5' end position. For each V gene, the 5' end position bound is set to the farthest position that is present in at least 95% of the annotated sequences. The potential novel alleles are then screened for a potential chimera event. The passed alleles are added to the germline reference and the repertoire is re-annotated.

4. Genotype inference using [TIgGER inferGenotypeBayesian](http://tigger.readthedocs.io/) and creating a personal germline reference set. The repertoire is re-annotated and double chromosome deletions and haplotypes are inferred using RAbHIT [deletionsByBinom and createFullHaplotype](https://yaarilab.bitbucket.io/RAbHIT/_).

5. OGRDB reports using [ogrdbstats](https://github.com/airr-community/ogrdbstats). 

# Docker version

A docker for the pipeline is available. The docker is based on the [immcantation container](https://immcantation.readthedocs.io/en/stable/docker/intro.html) with the added VDJbase pipeline.

## Docker

```bash
docker pull peresay/vdjbase_pipeline
```

## Using the Container

### Shell inside the container

Same as in immcantation container, you can invoke a shell using the following

```bash
docker run -it peresay/vdjbase_pipeline bash
```

### Sharing files with the container

Build in the same way as the immcantation container, the same four mount points are available:

```
/data
/scratch
/software
/oasis
```

### Geno-Haplo pipeline: vdjbase-pipeline
Runs the vdjbase-pipeline, the are several parameters that can be modified. For the full list please go to the help guide within the docker.

```bash
# Arguments
DATA_DIR=~/P1
FASTA_FILE=/data/P1_I1_S1.fasta
SAMPLE_NAME=P1_I1_S1
NPROC=4

# Run pipeline for all avialble parameters
docker run peresay/vdjbase_pipeline -h

# Run pipeline for fasta file in docker image
docker run -v $DATA_DIR:/data:z peresay/vdjbase_pipeline \
    vdjbase-pipeline -f $FASTA_FILE -s $SAMPLE_NAME -t $NPROC

AIRR_FILE=/data/P1_I1_S1.tsv
# Run pipeline for airr format file in docker image. 
docker run -v $DATA_DIR:/data:z peresay/vdjbase_pipeline \
    vdjbase-pipeline -f $AIRR_FILE -s $SAMPLE_NAME -t $NPROC 

```

# Tools and parameters

Tool          | Version       | parameters notes
------------- | ------------- | -------------
IgBlast       | 1.16.0        | up to 10 V alignments, IMGT reference set from 20/08/2020 
Change-O      | 1.0.0         | gene sampling up to 500 sequences, DefineClones distance threshold of 0.2, 
findNovelAlleles        | 1.0.0         | custom pos_range, alpha = 0.05, j_max = 0.15, min_seq = 1, germline_min = 1, min_frac = 0.75.
inferGenotypeBayesian        | 1.0.0         | For all find_unmutated = FALSE; V priors 0.6, 0.4, 0.4, 0.35, 0.25, 0.25, 0.25, 0.25, 0.25; D and J - single assignment sequences with priors 0.5,0.5,0,0,0,0,0,0,0.
RAbHIT        | 0.5.0         | createFullHaplotype min_minor_fraction = 0.3.
ogrdbstats    | 0.2.0         | default parameters
collapse_seq  | 1.0.0         | -


# Copying

VDJbase pipeline is free for use under the CC BY-SA 4.0
