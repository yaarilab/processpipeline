#!/bin/bash

# General parameters with defaults
call="v_call"  # Default column name for V allele calls
seq="sequence_alignment"  # Default column name for aligned sequence
find_unmutated=false  # Whether to use germline_db to find unmutated samples
sample_name=""
# ASC specific parameters
single_assignment=false   # Consider sequences with single assignment
z_threshold=0  # Default Z-score threshold
germline_file=""  # Path to germline file, empty if none
asc_annotation=false  # Whether to use ASC annotation
output_dir="."  # Default output directory

# Process command-line arguments
while [[ "$#" -gt 0 ]]; do
    case $1 in
        --call) call="$2"; shift ;;
        --seq) seq="$2"; shift ;;
        --find_unmutated) find_unmutated=true ;;
        --single_assignment) single_assignment=true ;;
        --asc_annotation) asc_annotation=true ;;
        --z_threshold) z_threshold="$2"; shift ;;
        --germline_file) germline_file="$2"; shift ;;
        --allele_threshold_table_file) allele_threshold_table_file="$2"; shift ;;
        --airrFile) airrFile="$2"; shift ;;
        --sample_name) sample_name="$2"; shift ;;
        -o|--output_dir) output_dir="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

# Check for mandatory allele threshold table file
if [ -z "$allele_threshold_table_file" ]; then
    echo "Error: No allele threshold table supplied."
    exit 1
fi

# Check for mandatory airrFile
if [ -z "$airrFile" ]; then
    echo "Error: No AIRR file supplied."
    exit 1
fi

# Set up sample name if needed
sample_name_prefix=""
if [ "$sample_name" != "" ]; then
    sample_name_prefix="${sample_name}_"
fi

# Call the R script with the arguments
Rscript -e "
    suppressMessages(library(piglet, quietly = TRUE))
    suppressMessages(library(tigger, quietly = TRUE))
    suppressMessages(library(data.table, quietly = TRUE))
    suppressMessages(library(dplyr, quietly = TRUE))

    # Load allele threshold table
    allele_threshold_table <- fread('${allele_threshold_table_file}')

    # Load data and germline db if available
    data <- fread('${airrFile}', data.table=FALSE)
    germline_db <- if ('${germline_file}' != '') readIgFasta('${germline_file}') else NA

    # Set parameter values
    find_unmutated <- as.logical('${find_unmutated}')
    single_assignment <- as.logical('${single_assignment}')
    asc_annotation <- as.logical('${asc_annotation}')

    # Add novel alleles to the reference set
    if (length(germline_db) > 0 && any(grepl('_', names(germline_db)))) {
        alleles <- grep('_', names(germline_db), value=TRUE)
        for (a in alleles) {
            a_split <- unlist(strsplit(a, '_'))
            base_allele <- a_split[1]
            snps <- paste0(a_split[-1], collapse='_')
            base_threshold <- allele_threshold_table[asc_allele == base_allele,]
            if (nrow(base_threshold) != 0) {
                iuis_allele <- paste0(base_threshold[['allele']], '_', snps)
                base_threshold[['asc_allele']] <- a
                base_threshold[['allele']] <- iuis_allele
                allele_threshold_table <- rbind(allele_threshold_table, base_threshold)
            }
        }
    }

    # Infer genotype
    cat('Inferring genotype...', sep = '\n')
    geno <- piglet::inferGenotypeAllele(data, 
                                        allele_threshold_table = allele_threshold_table, 
                                        germline_db = germline_db, 
                                        find_unmutated = find_unmutated,
                                        asc_annotation = asc_annotation,
                                        call = '${call}',
                                        seq = '${seq}',
                                        single_assignment = single_assignment)
    cat('Writing genotype report...', sep = '\n')
    # Write genotype report
    write.table(geno, file = paste0('${output_dir}','/','${sample_name_prefix}','${call}', '_genotype_report.tsv'), row.names=FALSE, sep='\t')

    if(!is.na(germline_db)){
        cat('Writing personal reference...', sep = '\n')
        # Filter genotypes by z_score
        geno <- geno[geno[['z_score']] >= ${z_threshold}]

        # Create personal reference set
        NOTGENO.IND <- !(sapply(strsplit(names(germline_db), '*', fixed=TRUE), '[', 1) %in% geno[['gene']])
        germline_db_new <- germline_db[NOTGENO.IND]
        for (i in 1:nrow(geno)) {
            allele <- geno[['allele']][i]
            IND <- names(germline_db) %in% allele
            germline_db_new <- c(germline_db_new, germline_db[IND])
        }

        # Write IMGT gapped fasta reference
        writeFasta(germline_db_new, file = paste0('${output_dir}','/','${sample_name_prefix}', '${call}', '_personal_reference.fasta'))
    }
"
