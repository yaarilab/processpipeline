suppressMessages(library(seqinr))
suppressMessages(library(tools))
suppressMessages(library(tigger))
suppressMessages(library(optparse))
suppressMessages(library(stringr))
suppressMessages(library(dplyr))
suppressMessages(library(data.table))
suppressMessages(library(alakazam))
suppressMessages(library(reshape2))
suppressMessages(library(shazam))
suppressMessages(library(rabhit))
suppressMessages(library(parallel))
suppressMessages(library(foreach))
suppressMessages(library(stringi))
suppressMessages(library(utils))
library(ogrdbstats)
files <-
  list.files(
    "/work/peresay/vdjbase/V9/",
    "ogrdb_plots",
    recursive = T,
    full.names = T
  )
chain = "IGH"
for (file in files) {
  out_path <- dirname(file)
  sample_name <- gsub("_ogrdb_plots.pdf", "", basename(file))
  print(sample_name)
  
  short <- ifelse(grepl(pattern = "(P5|P6|P2|P13|P15)", file), T, F)
  
  if(short) next()
  if(!grepl("P14", file)) next()

  source("/localdata/peresay/scripts/constant_parameters_vdjbase2.R")

  
  df <-
    read.delim(paste0(out_path, "/", sample_name, '.tsv'),
               stringsAsFactors = F)
  
  v_calls <-
    unique(sapply(strsplit(unique(df$v_call), ","), "[", 1))
  v_calls <- grep("_[A-Z][0-9]+[A-Z]", v_calls, value = T)
  print(paste0(v_calls, collapse=","))
  if(file.exists(paste0(out_path,"/",sample_name, "_novel_dataframe.tsv"))){
    novel_df <- read.delim(paste0(out_path,"/",sample_name, "_novel_dataframe.tsv"), stringsAsFactors = F)
    
    if(short){
        novel_df$polymorphism_call2 <-
        stringr::str_replace_all(novel_df$polymorphism_call,
                                 setNames(fr1_names$v_call_new, fr1_names$v_call))
        novel_df <- novel_df[novel_df$polymorphism_call2 %in% v_calls, ]
    }else{
        novel_df <- novel_df[novel_df$polymorphism_call %in% v_calls, ] 
    }
    print(paste0(novel_df$polymorphism_call, collapse=","))
    novels <- setNames(novel_df$novel_imgt, novel_df$polymorphism_call)
    #novels <- gsub("N",".",novels)
  }else{
    novels <- c()
  }

  if (file.exists(paste0(out_path, "/", sample_name, "_novel_gapped.fasta"))) {
    system(paste0(
      "rm -r ",
      paste0(out_path, "/", sample_name, "_novel_gapped.fasta")
    ))
  } else{
    
  }
  
  ## create repo for stats
  if (!chain %in% c("IGL", "IGK", "TRA")) {
    tigger::writeFasta(c(VGERM, DGERM, JGERM),
                       paste0(out_path, "/", sample_name, "_makedb_ref.fasta"))
  } else{
    tigger::writeFasta(c(VGERM, JGERM),
                       paste0(out_path, "/", sample_name, "_makedb_ref.fasta"))
  }
  MAKEDBREPO <-
    paste0(out_path, "/", sample_name, "_makedb_ref.fasta")
  
  if (short) {
    if (length(novels) != 0) {
      names(VGERM) <-
        stringr::str_replace_all(names(VGERM),
                                 setNames(fr1_names$v_call_new, fr1_names$v_call))
      names(novels) <-
        stringr::str_replace_all(names(novels),
                                 setNames(fr1_names$v_call_new, fr1_names$v_call))
      for (id in 1:length(novels)) {
        seq = gsub("N", ".", toupper(novels[id]))
        len <- ceiling(nchar(seq) / 3) * 3
        codons <-
          substring(seq, seq(1, len - 2, 3), seq(3, len, 3))
        gaps_lengths <- nchar(gsub("[^\\.\\-]", "", codons))
        
        id_t <- which(gaps_lengths %in% c(1, 2))
        codons[id_t] <- gsub("[A-Z]", ".", codons[id_t])
        #print(toupper(paste0(codons, collapse = "")))        
        novels[id] <- toupper(paste0(codons, collapse = ""))
      }
    }
    ref <- read.fasta(MAKEDBREPO, as.string = T)
    names(ref) <-
      stringr::str_replace_all(names(ref),
                               setNames(fr1_names$v_call_new, fr1_names$v_call))
    for (id in 1:length(ref)) {
      seq = gsub("N", ".", toupper(ref[id]))
      len <- ceiling(nchar(seq) / 3) * 3
      codons <-
        substring(seq, seq(1, len - 2, 3), seq(3, len, 3))
      gaps_lengths <- nchar(gsub("[^\\.\\-]", "", codons))
      
      id_t <- which(gaps_lengths %in% c(1, 2))
      codons[id_t] <- gsub("[A-Z]", ".", codons[id_t])
      #print(toupper(paste0(codons, collapse = "")))
      ref[id] <- toupper(paste0(codons, collapse = ""))
    }
    
    write.fasta(
      sequences = as.list(toupper(ref)),
      names = names(ref),
      paste0(out_path, "/", sample_name, "_makedb_ref.fasta"),
      open = "w"
    )
    MAKEDBREPO = paste0(out_path, "/", sample_name, "_makedb_ref.fasta")
  }else{

    ref <- read.fasta(MAKEDBREPO, as.string = T)
    #names(ref) <-
    #  stringr::str_replace_all(names(ref),
    #                           setNames(fr1_names$v_call_new, fr1_names$v_call))
    for (id in 1:length(ref)) {
      seq = gsub("N", ".", toupper(ref[id]))
      len <- ceiling(nchar(seq) / 3) * 3
      codons <-
        substring(seq, seq(1, len - 2, 3), seq(3, len, 3))
      gaps_lengths <- nchar(gsub("[^\\.\\-]", "", codons))

      id_t <- which(gaps_lengths %in% c(1, 2))
      codons[id_t] <- gsub("[A-Z]", ".", codons[id_t])
      #print(toupper(paste0(codons, collapse = "")))
      ref[id] <- toupper(paste0(codons, collapse = ""))
    }

    write.fasta(
      sequences = as.list(toupper(ref)),
      names = names(ref),
      paste0(out_path, "/", sample_name, "_makedb_ref.fasta"),
      open = "w"
    )
    MAKEDBREPO = paste0(out_path, "/", sample_name, "_makedb_ref.fasta")

  }

  if (length(novels) != 0)
    write.fasta(
      sequences = as.list(toupper(novels)),
      names = names(novels),
      paste0(out_path, "/", sample_name, "_novel_gapped.fasta"),
      open = "w"
    )
  novel_file <-
    ifelse(
      file.exists(paste0(
        out_path, "/", sample_name, "_novel_gapped.fasta"
      )),
      paste0(out_path, "/", sample_name, "_novel_gapped.fasta"),
      "-"
    )
  if (chain == "IGH") {
    setwd(out_path)
    generate_ogrdb_report(
      MAKEDBREPO,
      novel_file,
      "",
      paste0(out_path, "/", sample_name, '.tsv'),
      "IGHV",
      "IGHJ6",
      "V",
      "H",
      F
    )
    invisible(gc(verbose = FALSE))
    invisible(dev.off())
  } else{
    if (chain == "IGL") {
      setwd(out_path)
      ogrdbstats::generate_ogrdb_report(
        MAKEDBREPO,
        novel_file,
        "",
        paste0(out_path, "/", sample_name, '.tsv'),
        "IGKV",
        "IGKJ2",
        "V",
        "L",
        F
      )
      invisible(gc(verbose = FALSE))
      invisible(dev.off())
    } else{
      setwd(out_path)
      ogrdbstats::generate_ogrdb_report(
        MAKEDBREPO,
        novel_file,
        "",
        paste0(out_path, "/", sample_name, '.tsv'),
        "IGLV",
        "",
        "V",
        "L",
        F
      )
      invisible(gc(verbose = FALSE))
      invisible(dev.off())
    }
  }
  
}
